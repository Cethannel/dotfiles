#!/bin/bash

#xrandr --output HDMI-A-1 --mode 1920x1080 --right-of DisplayPort-0 --rate 60 &
#xrandr --output HDMI-A-1 --mode 2560x1440 --right-of DisplayPort-0 --rate 165 &
numlockx on &
#ibus-daemon -d &
fcitx5 &
# nitrogen --restore &
picom & # --config ~/.config/picom/picom.conf -b &
xet m 0 0 &
slstatus &
xsetwacom set $(xsetwacom --list devices | grep "[1-9]." -o | awk '{if(NR==1) print $0}') MapToOutput 2560x1440+0+0 &
xsetwacom set $(xsetwacom --list devices | grep "[1-9]." -o | awk '{if(NR==2) print $0}') Button 1 'key +ctrl z -ctrl' &
# trayer --edge top --widthtype percent --width 57 &
mbinpd &
dunst &
mpd &
# pipewire &
# alacritty --command doas sysctl -w abi.vsyscall32=0 &
/usr/libexec/polkit-gnome-authentication-agent-1 &
gentoo-pipewire-launcher &
#~/.dwm/pipewire.sh &
~/.dwm/launch_polybar.sh &
#/home/ethan/.local/bin/wallpaper-way.sh
feh --bg-scale ~/Pictures/ethan-wallpapers/Astolfo_moutain.jpg &
