#!/bin/bash

xrandr --output DisplayPort-0 --mode 2560x1440 --pos 0x0 --primary --rate 144 &
xrandr --output DisplayPort-1 --mode 2560x1440 --rate 60 --pos 2560x0 &
$HOME/.dwm/wallpaper.sh
