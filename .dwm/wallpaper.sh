#!/bin/bash
image=$(find ~/Pictures/ethan-wallpapers/ -maxdepth 1 -regextype egrep -regex ".*(png|jpg)" -type f | shuf -n 1)
echo $(find ~/Pictures/ethan-wallpapers/ -regex ".*(png|jpg)" -type f)
echo $image
feh --bg-fill $image &
