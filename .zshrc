if [[ -r "${XDG_CHACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]];
then
	source "${XDG_CHACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

if [ ! -d "$ZINIT_HOME" ]; then
	mkdir -p "$(dirname $ZINIT_HOME)"
	git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi

source "${ZINIT_HOME}/zinit.zsh"

autoload -U compinit && compinit

zinit ice depth=1; zinit light romkatv/powerlevel10k

zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions

zinit cdreplay -q

[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"

export PATH=/usr/bin:$HOME/.local/bin:$HOME/bin:/usr/local/bin:$PATH
export PATH=$HOME/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/bin:$PATH
export PATH=$PATH:$HOME/go/bin
export PATH=$HOME/.local/share/exports/bin:$PATH
export PATH=$HOME/.cargo/bin:$PATH
export EDITOR="nvim"

export NIX_CONF_DIR="$HOME/.config/nix"

export LANG=en_US.UTF-8

alias doas="doas --"

alias ssk="ssh root@kya-server"
alias sse="ssh root@ethan-server"

alias ..='cd ..' 
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

alias v='nvim'

alias ls='eza -al --color=always --group-directories-first' # my preferred listing
alias la='eza -a --color=always --group-directories-first'  # all files and dirs
alias ll='eza -l --color=always --group-directories-first'  # long format
alias lt='eza -aT --color=always --group-directories-first' # tree listing
alias l.='eza -a | egrep "^\."'

alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB

alias py='python3'

alias config='git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

# pnpm
export PNPM_HOME="$HOME/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end


export XDG_CURRENT_DESKTOP=hyprland
export MOZ_ENABLE_WAYLAND=1

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm


export PICO_SDK_PATH=$HOME/SSD2/Code/Pinpoint/pico-sdk
export PICO_EXTRAS_PATH=$HOME/SSD2/Code/Pinpoint/pico-extras
export RUSTC_WRAPPER=sccache

if [ -e /home/ethan/.nix-profile/etc/profile.d/nix.sh ]; then . /home/ethan/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
[[ ! -r /home/ethan/.opam/opam-init/init.zsh ]] || source /home/ethan/.opam/opam-init/init.zsh  > /dev/null 2> /dev/null


[ -f "/home/ethan/.ghcup/env" ] && . "/home/ethan/.ghcup/env" # ghcup-env