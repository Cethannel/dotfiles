require("luasnip.session.snippet_collection").clear_snippets "go"

local ls = require "luasnip"

local s = ls.snippet
local i = ls.insert_node
local t = ls.text_node

--local fmt = require("luasnip.extras.fmt").fmt

ls.add_snippets("go", {
  s("rerr", {
    t "if err != nil {",
    t "return err",
    t "}",
  }),
  s("rferr", {
    t "if err != nil {",
    t 'return fmt.Errorf("',
    i(0),
    t '", ',
    i(1),
    t ")",
    t "}",
  }),
})
