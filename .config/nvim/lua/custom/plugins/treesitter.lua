return {
  {
    "nvim-treesitter/nvim-treesitter",
    dependencies = {
      { dir = "~/Git/tree-sitter-lua" },
    },
    branch = "master",
    lazy = false,
    config = function()
      require("custom.treesitter").setup()
    end,
  },
}
