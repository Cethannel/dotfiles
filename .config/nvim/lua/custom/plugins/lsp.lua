return {
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      "folke/neodev.nvim",
      "williamboman/mason.nvim",
      "williamboman/mason-lspconfig.nvim",
      "WhoIsSethDaniel/mason-tool-installer.nvim",
      "stevearc/conform.nvim",
    },
    config = function()
      require("neodev").setup {}

      local capabilities = nil
      if pcall(require, "cmp_nvim_lsp") then
        capabilities = require("cmp_nvim_lsp").default_capabilities()
        capabilities.textDocument.completion.completionItem.snippetSupport = true
      end

      vim.env.TEMPL_EXPERIMENT = "rawgo"

      local servers = {
        lua_ls = true,
        gopls = true,
        templ = true,
        rust_analyzer = true,
        ocamllsp = true,
        pyright = true,
        zls = {
          manual_install = true,
          settings = {
            zls = {
              --enable_build_on_save = true,
            },
            Lua = {
              format_on_save = false,
            },
          },
        },
        wgsl_analyzer = true,
        --clangd = {
        --  init_options = { clangdFileStatus = true },
        --  filetypes = { "c", "cpp" },
        --},
        hls = {
          manual_install = true,
          cmd = { "haskell-language-server-wrapper", "--lsp" },
        },
        gleam = {
          manual_install = true,
        },
        eslint = true,
        ts_ls = true,
        elmls = true,
        svelte = true,
        sqlls = true,
        scheme_langserver = {
          manual_install = true,
        },
        ols = true,
        phpactor = true,
        texlab = true,
        tailwindcss = {
          filetypes = {
            "css",
            "html",
            "scss",
            "templ",
          },
        },
        cssls = true,
        ccls = {
          manual_install = true,
        },
        glsl_analyzer = true,
        html = true,
        htmx = true,
        jdtls = true,
      }

      for _, method in ipairs { "textDocument/diagnostic", "workspace/diagnostic" } do
        local default_diagnostic_handler = vim.lsp.handlers[method]
        vim.lsp.handlers[method] = function(err, result, context, config)
          if err ~= nil and err.code == -32802 then
            return
          end
          return default_diagnostic_handler(err, result, context, config)
        end
      end

      local servers_to_install = vim.tbl_filter(function(key)
        local t = servers[key]
        if type(t) == "table" then
          return not t.manual_install
        else
          return t
        end
      end, vim.tbl_keys(servers))

      require("mason").setup()

      local ensure_installed = {
        "stylua",
        "lua_ls",
        "delve",
        -- "tailwind-language-server",
      }

      vim.list_extend(ensure_installed, servers_to_install)
      require("mason-tool-installer").setup { ensure_installed = ensure_installed }

      local lspconfig = require "lspconfig"

      for name, config in pairs(servers) do
        if config == true then
          config = {}
        end
        config = vim.tbl_deep_extend("force", {}, {
          capabilities = capabilities,
        }, config)

        lspconfig[name].setup(config)
      end

      local disable_semantic_tokens = {
        lua = true,
      }

      vim.api.nvim_create_autocmd("LspAttach", {
        callback = function(args)
          local bufnr = args.buf
          local client = assert(vim.lsp.get_client_by_id(args.data.client_id), "must have valid client")

          vim.opt_local.omnifunc = "v:lua.vim.lsp.omnifunc"
          vim.keymap.set("n", "gd", vim.lsp.buf.definition, { buffer = 0 })
          vim.keymap.set("n", "gr", vim.lsp.buf.references, { buffer = 0 })
          vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { buffer = 0 })
          vim.keymap.set("n", "gT", vim.lsp.buf.type_definition, { buffer = 0 })
          vim.keymap.set("n", "K", vim.lsp.buf.hover, { buffer = 0 })

          vim.keymap.set("n", "<leader>gr", vim.lsp.buf.rename, { buffer = 0 })
          vim.keymap.set("n", "<leader>ga", vim.lsp.buf.code_action, { buffer = 0 })
          vim.keymap.set("n", "<leader>gf", vim.lsp.buf.format, { buffer = 0 })

          local filetype = vim.bo[bufnr].filetype
          if disable_semantic_tokens[filetype] then
            client.server_capabilities.semanticTokensProvider = nil
          end
        end,
      })

      local noFormatOnSave = {
        c = true,
        cpp = true,
        h = true,
        hpp = true,
        glsl = true,
      }

      require("conform").setup {
        formatters_by_ft = {
          lua = { "stylua" },
          zig = { nil },
          glsl = { nil },
        },
        format_on_save = function(bufnr)
          if
            vim.tbl_contains(vim.tbl_keys(noFormatOnSave), function(v)
              return v == vim.bo.filetype
            end, { predicate = true })
          then
            return nil
          end
          return {
            -- I recommend these options. See :help conform.format for details.
            lsp_format = "fallback",
            timeout_ms = 500,
          }
        end,
      }

      vim.api.nvim_create_autocmd("BufWritePre", {
        pattern = { "^[^.]+$|\\.(?!(c|cpp|h|hpp)$)([^.]+$)" },
        callback = function(args)
          require("conform").format {
            bufnr = args.buf,
            lsp_fallback = true,
            quiet = true,
          }
        end,
      })
    end,
  },
}
