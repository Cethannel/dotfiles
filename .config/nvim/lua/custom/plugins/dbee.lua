return {
  {
    "kndndrj/nvim-dbee",
    enabled = false,
    dependencies = { "MunifTanjim/nui.nvim" },
    build = function()
      require("dbee").install()
    end,
    config = function()
      --local source = require "dbee.sources"
      require("dbee").setup {}
      require "custom.dbee"
    end,
  },
}
