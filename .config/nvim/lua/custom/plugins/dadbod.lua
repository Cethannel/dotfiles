return {
	'kristijanhusak/vim-dadbod-ui',
	'tpope/vim-dadbod',
	'kristijanhusak/vim-dadbod-completion',
	cmd = {
		'DBUI',
		'DBUIToggle',
		'DBUIAddConnection',
		'DBUIFindBuffer',
	},
	init = function()
		-- Your DBUI configuration
		vim.g.db_ui_use_nerd_fonts = 1

		vim.g.dbs = {
			{ name = 'sage', url = 'sqlite3:///home/ethan/.cache/sage_sql/project.sql' }
		}
	end,
}
