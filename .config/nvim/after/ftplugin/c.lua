local set = vim.opt_local

set.shiftwidth = 2
vim.opt_local.formatoptions:remove "o"
