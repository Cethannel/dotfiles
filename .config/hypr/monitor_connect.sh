#!/bin/zsh

handle() {
  case $1 in monitoradded*)
    hyprctl dispatch moveworkspacetomonitor "1 1"
    hyprctl dispatch moveworkspacetomonitor "2 1"
    hyprctl dispatch moveworkspacetomonitor "4 1"
    hyprctl dispatch moveworkspacetomonitor "5 1"
  esac
}

socat - UNIX-CONNECT:/tmp/hypr/51a930f802c71a0e67f05e7b176ded74e8e95f87_1687983125/.socket2.sock | while read -r line; do handle "$line"; done

